% Interactive version of getTracks.m
% Track Z rings in epifluorescence time lapses
%
% Program will promt for image and pill mesh data
% 
% User can step through the pipeline and view various aspects of the
% results

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu


%% Parameter selection

% =========================== 
%       Key Parameters
% ===========================

% cell tracks
minLength = 20; % minimum track length for cell tracks
minGof = 0.99; % minimum r squared for linearity for cell tracks

% peak tracking
linkingDist = 5; % maximum linking distance for peaks, in pixels
linkingTime = 5; % maximum linking time for peaks, in frames
minTrackLength = 20; % minimum peak track length 
minTrackGof = 0; % minimum r squared for linearity for peak tracks

%% Initialization 

% Add bioformats to path
addpath([pwd,'/bfmatlab/'])

% Initialize bioformats
status = bfCheckJavaPath;
assert(status, ['Missing Bio-Formats library. Either add bioformats_package.jar '...
    'to the static Java path or add it to the Matlab path.']);

%% Load files

% open single molecule time lapse
[file, path] = uigetfile(bfGetFileExtensions,'Open image file',path);
if file == 0
    return
end
img = bfopen(fullfile(path,file));
img = cat(3,img{1}{:,1});

% open pill mesh file
[file, path] = uigetfile('*pill_MESH.mat','Open image file',path);
if file == 0
    return
end
pillMesh = open(fullfile(path,file));
frame = pillMesh.frame;
Ncell = pillMesh.Ncell;

% parse cell tracks from pill mesh data
% each element is [frame object] 
tracks = cell(Ncell,1);
for i = 1:Ncell
    for j = 1:length(frame)
        currFrame = frame(j).object;
        for k = 1:length(currFrame)
            if currFrame(k).cellID == i
                tracks{i}(end+1,:) = [j,k];
            end
        end
    end
end

%% view cell tracks as midlines

figure;
% color cell midlines persistently
colors = jet(10); colors = repmat(colors,20,1);
% adjust image intensity
temp = double(img(:,:,1))./2^16;
imScales = [prctile(temp(:),0),prctile(temp(:),99.99)];
% plot cell tracks for each frame
for i = 1:size(img,3)
    imshow(imadjust(img(:,:,i),imScales))
    hold on
    for j = 1:length(tracks)
        currTrack = tracks{j};
        % length filter
        if size(currTrack,1) < minLength
            continue
        end
        if currTrack(1,1) <= i && currTrack(end,1) >= i
            n = find(currTrack(:,1)==i);
            if ~isempty(n)
                currFrame = currTrack(n,1);
                currObj = currTrack(n,2);
                if ~isempty(frame(currFrame).object(currObj).centerline)
                    plot(frame(currFrame).object(currObj).centerline(:,1),frame(currFrame).object(currObj).centerline(:,2),...
                        'Color',colors(j,:));
                end
            end
        end
    end
    pause(0.1)
    hold off
end

%% filter, generate intensity traces, find and track Z rings

% structure to store data
allTracks = struct();

for i = 1:length(tracks)
    
    currTrack = tracks{i};
    
    % holding variables
    kymograph = cell(1,size(currTrack,1));
    peaks = [];
    pbf = {};
    peakTracks = {};
    cellImages = {};
    
    % length filter
    if size(currTrack,1) < minLength
        continue
    end
    
    % COMPUTE INTENSITY AND FIND PEAKS
    
    for ii = 1:size(currTrack,1)
        
        % get mesh
        currImg = img(:,:,currTrack(ii,1));
        currCell = frame(currTrack(ii,1)).object(currTrack(ii,2));
        currMesh = currCell.pill_mesh;
        
        % skip non-meshed cells
        if isempty(currMesh)
            kymograph{ii} = 0;
            continue
        end
        
        currMesh = currMesh(5:end-4,:); % remove poles
        
        % compute intensity trace
        intensityTrace = zeros(size(currMesh,1),1);
        cellImage = zeros(size(currMesh,1),10);
        for j = 1:length(currMesh)
            % get mean intensity along spline
            xcoords = round(linspace(currMesh(j,1),currMesh(j,3),10));
            ycoords = round(linspace(currMesh(j,2),currMesh(j,4),10));
            intensityValues = zeros(10,1);
            for k = 1:10
                intensityValues(k) = currImg(ycoords(k),xcoords(k));
            end
            intensityTrace(j) = mean(intensityValues);
            cellImage(j,:) = intensityValues';
        end
        % save and normalize
        kymograph{ii} = intensityTrace;%-prctile(intensityTrace,10);
        cellImages{ii} = cellImage;
        
        if ~isempty(intensityTrace)
            % peak finding
            [~,locs,w] = findpeaks(smooth(intensityTrace,10),'WidthReference','halfprom',...
            'MinPeakDistance',min(20,length(intensityTrace)-2),'MinPeakProminence',40);
            % save peak statistics
            for j = 1:length(locs)
                peaks(end+1,:) = [ii,locs(j),w(j)];
            end
            pbf{ii} = [locs,w];
        end
        
    end
    
    % filter for linearity of cell growth 
    gOfF = regstats(cellfun(@length,kymograph),1:length(kymograph),'linear','rsquare');
    if gOfF.rsquare < minGof
        continue
    end
    
    % center peaks
    for ii = 1:size(pbf,2)
        offset = floor(max(cellfun(@length,kymograph))/2-length(kymograph{ii})/2)+1;
        if ~isempty(pbf{ii})
            pbf{ii}(:,1) = pbf{ii}(:,1)+offset;
        end
    end
    
    % TRACK PEAKS
    
    % initialize tracks
    peakTracks = {};

    % nearest neighbor linking to build tracks
    for ii = 1:length(pbf)
        % make new tracks holder
        peakTracks2 = {};
        % skip tracks that are past linking time cutoff
        for j = length(peakTracks):-1:1
            if (ii-peakTracks{j}(end,1)) > linkingTime
                peakTracks2{end+1} = peakTracks{j};
                peakTracks(j) = [];
            end
        end
        currPoints = pbf{ii};
        
        done = false;
        % if no unlinked tracks, no need to link 
        if isempty(currPoints) || isempty(peakTracks)
            done = true;
        end
        
        while ~done

            % compute pairwise distances and find nearest
            oldPositions = cellfun(@(x) x(end,2),peakTracks);
            newPositions = currPoints(:,1);
            D = pdist2(oldPositions',newPositions);
            % done if max linking distance is reached
            if min(min(D)) > linkingDist
                done = true;
            % update tracks
            else
                [oldI,newI] = find(D==min(min(D)));
                peakTracks2{end+1} = [peakTracks{oldI(1)};[ii,currPoints(newI(1),:)]];
                peakTracks(oldI(1)) = [];
                currPoints(newI(1),:) = [];
            end
            % stop if all points linked
            if isempty(currPoints) || isempty(peakTracks)
                done = true;
            end
            
        end
        
        % save unlinked tracks
        peakTracks2 = [peakTracks2,peakTracks];
        
        % turn unlinked points into new tracks
        for j = 1:size(currPoints,1)
            peakTracks2{end+1} = [ii,currPoints(j,:)];
        end
        
        % update
        peakTracks = peakTracks2;
        
    end
    
    % track length filter
    peakTracks = peakTracks(cellfun(@length,peakTracks)>minTrackLength);
    
    % track linearity filter
    for j = length(peakTracks):-1:1
        gOfF = regstats(peakTracks{j}(:,1),peakTracks{j}(:,2),'linear','rsquare');
        if gOfF.rsquare < minTrackGof
            peakTracks(j) = [];
        end
    end
    
    % store results
    allTracks(end+1).track = currTrack;
    allTracks(end).kymograph = kymograph;
    allTracks(end).peaks = peaks;
    allTracks(end).peakTracks = peakTracks;
    allTracks(end).cellImages = cellImages;
end

% remove empty first element
allTracks = allTracks(2:end);

%% view cell kymographs

figure
for i = 1:length(allTracks)
    kymograph = allTracks(i).kymograph;
    % generate kymograph image
    desmoImg = ones(length(kymograph),max(cellfun(@length,kymograph)));
    desmoImg = desmoImg.*min(cellfun(@min,kymograph));
    for j = 1:length(kymograph)
        indMin = floor(size(desmoImg,2)/2-length(kymograph{j})/2)+1;
        indMax = floor(size(desmoImg,2)/2+length(kymograph{j})/2);
        desmoImg(j,indMin:indMax) = kymograph{j};
    end
    % plot kymograph image
    imagesc(desmoImg); axis equal tight; colormap gray
    pause(1)
end

%% view cell kymographs with tracked Z rings 

figure
for i = 1:length(allTracks)
    kymograph = allTracks(i).kymograph;
    peakTracks = allTracks(i).peakTracks;
    % generate kymograph image background
    desmoImg = ones(length(kymograph),max(cellfun(@length,kymograph)));
    desmoImg = desmoImg.*min(cellfun(@min,kymograph));
    % offset for centering
    offset = zeros(1,length(kymograph));
    % center kymograph image
    for j = 1:length(kymograph)
        indMin = floor(size(desmoImg,2)/2-length(kymograph{j})/2)+1;
        indMax = floor(size(desmoImg,2)/2+length(kymograph{j})/2);
        desmoImg(j,indMin:indMax) = kymograph{j};
        offset(j) = indMin;
    end
    % plot kymograph image
    hold off
    imagesc(desmoImg); axis equal tight; colormap gray
    hold on
    for j = 1:length(peakTracks)
        plot(peakTracks{j}(:,2),peakTracks{j}(:,1),'-','LineWidth',3);
    end
    pause(0.5)
end

%% show kymographs and plot peak widths

figure('Position',[154 346 1136 420]);
colors = parula(20);
for i = 1:length(allTracks)
    kymograph = allTracks(i).kymograph;
    peaks = allTracks(i).peaks;
    peakTracks = allTracks(i).peakTracks;
    % generate kymograph image background
    desmoImg = ones(length(kymograph),max(cellfun(@length,kymograph)));
    desmoImg = desmoImg.*min(cellfun(@min,kymograph));
    % offset for centering
    offset = zeros(1,length(kymograph));
    % center kymograph image
    for j = 1:length(kymograph)
        indMin = floor(size(desmoImg,2)/2-length(kymograph{j})/2)+1;
        indMax = floor(size(desmoImg,2)/2+length(kymograph{j})/2);
        desmoImg(j,indMin:indMax) = kymograph{j};
        offset(j) = indMin;
    end
    % plot kymograph image
    subplot(1,2,1)
    hold off
    imagesc(desmoImg); axis equal tight; colormap gray
    hold on
    % plot points
    for j = 1:size(peaks,1)
        colorInd = round(peaks(j,3))-5;
        if colorInd < 1
            colorInd = 1;
        elseif colorInd > 20
            colorInd = 20;
        end
        plot(peaks(j,2)+offset(peaks(j,1)),peaks(j,1),'.','MarkerSize',20,'Color',colors(colorInd,:));
    end
    % plot peak widths
    subplot(1,2,2)
    hold off
    for j = 1:length(peakTracks)
        plot(peakTracks{j}(:,1),peakTracks{j}(:,3),'LineWidth',1)
        hold on
    end
    xlabel('time (frames)'); ylabel('peak width (pixels)');
    pause(0.5)
end

%% plot Z ring kymographs

figure

for i = 1:length(allTracks)
    peakTracks = allTracks(i).peakTracks;
    cellImages = allTracks(i).cellImages;
    
    % center cell images
    offset = zeros(1,length(cellImages));
    % center kymograph image
    for j = 1:length(cellImages)
        indMin = floor(max(cellfun(@(x) size(x,1),cellImages))/2-size(cellImages{j},1)/2)+1;
        offset(j) = indMin;
    end
    for j = 1:length(peakTracks)
        currTrack = peakTracks{j};
        divisionKymo = [];
        for k = 1:length(currTrack)
            currFrame = currTrack(k,1);
            peakPos = currTrack(k,2);
            try
                ringSlice = cellImages{currFrame}(peakPos-offset(currFrame)-3:peakPos-offset(currFrame)+3,:);
                divisionKymo(end+1,:) = mean(ringSlice);
            catch
                divisionKymo = [];
                continue
            end
        end
        if ~isempty(divisionKymo)
            imagesc(divisionKymo);
            axis equal tight; colormap gray
            pause(0.5)
        end
    end
end
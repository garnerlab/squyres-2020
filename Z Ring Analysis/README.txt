README: Z ring analysis

WRITTEN BY: Georgia Squyres, Garner Lab, Harvard University
In MATLAB R2018b
Contact: squyres@g.harvard.edu 

ADDITIONAL DOWNLOADS: 

In order to run this code, you must additionally download BioFormats and add it to your MATLAB path:

bfmatlab: https://downloads.openmicroscopy.org/bio-formats/

INSTRUCTIONS FOR USE:

First, segment cells to generate cell masks using DeepCell or the cell segmentation program of your choice, and generate pill meshes for your cells using Morphometrics.  

To analyze Z rings in single images, run zRingAnalyzer.m. You will be prompted to load an image file and a pill mesh file from Morphometrics. Step through the rest of the pipeline to analyze Z rings, and use the graphing code at the end of the pipeline to view various results. Two results variables will be generated:

1: ringDistances: distances between neighboring Z rings

2: zRings: structure with fields:
   width: Z ring width (full width half max)
   height: Z ring intensity
   image: cropped image around the Z ring
   peak: intensity trace along the long axis of the cell, centered on the Z ring

To analyze Z rings over a time lapse, run zRingTimeAnalyzer.m. You will be prompted to load a time lapse image file and a pill mesh file from Morphometrics. Step through the rest of the pipeline to analyze Z rings, and use the graphing code at the end of the pipeline to view various results. The results are:

allTracks: data for Z rings, tracked from formation to division: structured array with the following fields:
   track: coordinates of each tracked cell from pill mesh data, in [time, object] format
   kymograph: intensity along the midline of the corresponding cell at each time point
   peaks: Z rings identified in each cell, in [time, coordinate along cell midline, width, height] format
   peakTracks: tracks of Z rings in each cell, in same format as peaks
   cellImages: images of cells at each time point

Z ring tracking is performed in the getTracks subfunction, which has several important parameters. It may be helpful to check these parameters and adjust them to your data set, so an interactive mode (getTracksInteractive.m) has been provided to make it possible to test the performance of this tracking. 
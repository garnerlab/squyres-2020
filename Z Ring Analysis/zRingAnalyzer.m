% Analyze the features of Z rings in epfluorescence images
%
% Program will prompt for an image file and a corresponding pill mesh file 
%
% Results: 
% ringDistances: distances between neighboring Z rings
% zRings: structure with fields:
%   width: Z ring width (full width half max)
%   height: Z ring intensity
%   image: cropped image around the Z ring
%   peak: intensity trace along the long axis of the cell, centered on the Z ring
%
% Code to display results is also provided

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

% =========================== 
%       Key Parameters
% ===========================

% range around each Z ring to align, in pixels
windowSize = 15;

% peak detection parameters
smoothFact = 10; % curve smoothing parameter
minDist = 20; % minimum distance between peaks
minProm = 40; % minimum peak prominence

% ===========================

% initialize results
ringDistances = [];
zRings = struct('width',[],'height',[],'image',[],'peak',[]);

% open image file
[file,path] = uigetfile({'*.tif'},'Open image file');
if file == 0
    return
end
img = imread(fullfile(path,file));

% open pill mesh file
[file,path] = uigetfile({'*.mat'},'Open mesh file');
if file == 0
    return
end
mesh = open(fullfile(path,file));
mesh = mesh.frame;

% overlay cell perimiters and centerlines on image
%{
figure; imshow(img,[])
hold on
for i = 1:size(mesh.object,2)
    currCell = mesh.object(i);
    currCenter = currCell.centerline;
    currMesh = currCell.pill_mesh;
    if ~ isequal(currCenter, [])
        plot(currCenter(:,1),currCenter(:,2),'g');
        plot(currCell.Xperim,currCell.Yperim,'g');
    end
end
%}

% Z ring analysis
for i = 1:size(mesh.object,2)

    % get current mesh
    currCell = mesh.object(i);
    currMesh = currCell.pill_mesh;
    if isempty(currMesh)
        continue
    end
    
    % remove poles
    currMesh = currMesh(5:end-4,:); 
    
    % initialization 
    intensityTrace = zeros(size(currMesh,1),1);
    cellImage = zeros(size(currMesh,1),10);

    % compute intensity trace along pill mesh splines
    for j = 1:length(currMesh)

        % convert spline into xy coordinates
        xcoords = round(linspace(currMesh(j,1),currMesh(j,3),10));
        ycoords = round(linspace(currMesh(j,2),currMesh(j,4),10));

        % get mean intensity along spline
        intensityValues = zeros(10,1);
        for k = 1:10
            intensityValues(k) = img(ycoords(k),xcoords(k));
        end
        intensityTrace(j) = mean(intensityValues);
        cellImage(j,:) = intensityValues';
    end

    % peak fitting
    if ~isempty(intensityTrace)

        % visualize intensity traces
        %{
        hold off
        plot(intensityTrace)
        hold on
        findpeaks(smooth(intensityTrace,smoothFact),...
            'MinPeakDistance',min(minDist,length(intensityTrace)-2),'MinPeakProminence',minProm);
        xlabel('Position'),ylabel('Intensity');
        set(gca,'FontSize',16);set(gca,'TickDir','out')
        pause(0.5)
        %}

        % peak finding
        % find alignment locations with smoothed peaks
        [~,locs,w,h] = findpeaks(smooth(intensityTrace,smoothFact),'WidthReference','halfprom',...
            'MinPeakDistance',min(minDist,length(intensityTrace)-2),'MinPeakProminence',minProm);

        % save peak distances
        if ~isempty(locs)
            ringDistances = [ringDistances;diff(locs)];
        end

        % align and save results
        for j = 1:length(locs)
            try % only if there's a large enough window size
                % grab image and peak in the specified window
                peakRegion = intensityTrace(locs(j)-windowSize:locs(j)+windowSize);
                imageRegion = cellImage(locs(j)-windowSize:locs(j)+windowSize,:);
                % save results
                zRings(end+1).peak = peakRegion;
                zRings(end).image = imageRegion;
                zRings(end).width = w(j);
                zRings(end).height = h(j);
            catch ME
                if ~strcmp(ME.identifier,'MATLAB:badsubscript')
                    rethrow(ME)
                end
            end
        end
    end
end

%% Show average Z ring projection

figure('NumberTitle','off','Name','Average Z ring projection'); 
imshow(mean(cat(3,zRings(:).image),3),[],'InitialMagnification',300);
    
%% Plot average Z ring peak

figure; hold on;
allPeaks = cat(2,zRings(:).peak);
% plot SEM as shaded area
SEM = std(allPeaks')./sqrt(size(allPeaks,2));
semX = (-1*windowSize:windowSize);
semX = [semX,fliplr(semX)];
semY = [mean(allPeaks,2)'+SEM,fliplr(mean(allPeaks,2)'-SEM)];
temp = fill(semX,semY,'b'); temp.LineStyle = 'none'; temp.FaceAlpha = 0.2;
% plot average intensity line
plot((-1*windowSize:windowSize),mean(allPeaks,2),'b','LineWidth',1); 
  
xlabel('Position (pixels)');
ylabel('Normalized intensity');
title('Average Z ring peak');
    
%% Compute average Z ring width

allPeaks = cat(2,zRings(:).peak);

bootFwhm = zeros(1000,1);
% Compute FWHM
[~,~,w,~] = findpeaks(mean(allPeaks,2));
fwhm = max(w);
% Bootstrap error
bootstat = bootstrp(1000,@(x) mean(x,2),allPeaks);
for j = 1:size(bootstat,1)
    [~,~,w] = findpeaks(bootstat(j,:));
    bootFwhm(j) = max(w);
end

disp(['Z ring width: ',num2str(round(fwhm,2)),' +/- ',num2str(round(std(bootFwhm),2)),' pixels']);

%% Plot histogram of Z ring spacing

figure; hold on

[n,edges] = histcounts(ringDistances,'Normalization','Probability');
plot(mean([edges(1:end-1);edges(2:end)]),n,'-','LineWidth',1);

temp = xlabel('Distance between Z rings (pixels)');
temp = ylabel('Normalized frequency');
title('Distance between Z rings');

% Track Z rings in epifluorescence time lapses
%
% Inputs:
% img: Time lapse of Z rings
% frame: frame data from pill mesh
% Ncell: Ncell data from pill mesh
%
% Results: 
% allTracks: data for Z rings, tracked from formation to division
% structured array with the following fields:
%    track: coordinates of each tracked cell from pill mesh data, in 
%      [time, object] format
%    kymograph: intensity along the midline of the corresponding cell at
%      each time point
%    peaks: Z rings identified in each cell, in [time, coordinate along
%      cell midline, width, height] format
%    peakTracks: tracks of Z rings in each cell, in same format as peaks
%    cellImages: images of cells at each time point 

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

function allTracks = getTracks(img,frame,Ncell)

%% Parameter selection 

% =========================== 
%       Key Parameters
% ===========================

% cell tracks
minLength = 20; % minimum track length for cell tracks
minGof = 0.99; % minimum r squared for linearity for cell tracks

% peak tracking
linkingDist = 5; % maximum linking distance for peaks, in pixels
linkingTime = 5; % maximum linking time for peaks, in frames
minTrackLength = 20; % minimum peak track length 
minTrackGof = 0; % minimum r squared for linearity for peak tracks

%% parse tracks from frame data
% each element is [frame object] 
tracks = cell(Ncell,1);
for i = 1:Ncell
    for j = 1:length(frame)
        currFrame = frame(j).object;
        for k = 1:length(currFrame)
            if currFrame(k).cellID == i
                tracks{i}(end+1,:) = [j,k];
            end
        end
    end
end

%% filter, generate intensity traces, find and track peaks

% structure to store data
allTracks = struct();

for i = 1:length(tracks)
    
    currTrack = tracks{i};
    
    % holding variables
    kymograph = cell(1,size(currTrack,1));
    peaks = [];
    peakDistances = [];
    pbf = {};
    peakTracks = {};
    cellImages = {};
    
    % LENGTH FILTER
    if length(currTrack) < minLength
        continue
    end
    
    % COMPUTE INTENSITY AND FIND PEAKS
    
    for ii = 1:size(currTrack,1)
        
        % get mesh
        currImg = img(:,:,currTrack(ii,1));
        currCell = frame(currTrack(ii,1)).object(currTrack(ii,2));
        currMesh = currCell.pill_mesh;
        
        % skip non-meshed cells
        if isempty(currMesh) || size(currMesh,1) < 20
            kymograph{ii} = 0;
            continue
        end
        
        currMesh = currMesh(5:end-4,:); % remove poles
        
        % compute intensity trace
        intensityTrace = zeros(size(currMesh,1),1);
        cellImage = zeros(size(currMesh,1),10);
        for j = 1:size(currMesh,1)
            % get mean intensity along spline
            xcoords = round(linspace(currMesh(j,1),currMesh(j,3),10));
            ycoords = round(linspace(currMesh(j,2),currMesh(j,4),10));
            intensityValues = zeros(10,1);
            for k = 1:10
                intensityValues(k) = currImg(ycoords(k),xcoords(k));
            end
            intensityTrace(j) = mean(intensityValues);
            cellImage(j,:) = intensityValues';
        end
        % save and normalize
        kymograph{ii} = intensityTrace-prctile(intensityTrace,10);
        cellImages{ii} = cellImage;
        
        if ~isempty(intensityTrace)
            % peak finding
            [~,locs,w,h] = findpeaks(smooth(intensityTrace,10),'WidthReference','halfprom',...
            'MinPeakDistance',min(20,length(intensityTrace)-2),'MinPeakProminence',40);
            % save peak statistics
            for j = 1:length(locs)
                peaks(end+1,:) = [ii,locs(j),w(j),h(j)];
            end
            peakDistances = [peakDistances;diff(locs)];
            pbf{ii} = [locs,w,h];
        end
        
    end
    
    % filter for linearity of cell growth 
    gOfF = regstats(cellfun(@length,kymograph),1:length(kymograph),'linear','rsquare');
    if gOfF.rsquare < minGof
        continue
    end
    
    % TRACK PEAKS
    
    % initialize tracks
    peakTracks = {};

    % nearest neighbor linking to build tracks
    for ii = 1:length(pbf)
        % make new tracks holder
        peakTracks2 = {};
        % skip tracks that are past length cutoff
        for j = length(peakTracks):-1:1
            if (ii-peakTracks{j}(end,1)) > linkingTime
                peakTracks2{end+1} = peakTracks{j};
                peakTracks(j) = [];
            end
        end
        currPoints = pbf{ii};
        
        done = false;
        % if no unlinked tracks, no need to link 
        if isempty(currPoints) || isempty(peakTracks)
            done = true;
        end
        
        while ~done

            % compute pairwise distances and find nearest
            oldPositions = cellfun(@(x) x(end,2),peakTracks);
            newPositions = currPoints(:,1);
            D = pdist2(oldPositions',newPositions);
            % done if max linking distance is reached
            if min(min(D)) > linkingDist
                done = true;
            % update tracks
            else
                [oldI,newI] = find(D==min(min(D)));
                peakTracks2{end+1} = [peakTracks{oldI(1)};[ii,currPoints(newI(1),:)]];
                peakTracks(oldI(1)) = [];
                currPoints(newI(1),:) = [];
            end
            % stop if all points linked
            if isempty(currPoints) || isempty(peakTracks)
                done = true;
            end
            
        end
        
        % save unlinked tracks
        peakTracks2 = [peakTracks2,peakTracks];
        
        % turn unlinked points into new tracks
        for j = 1:size(currPoints,1)
            peakTracks2{end+1} = [ii,currPoints(j,:)];
        end
        
        % update
        peakTracks = peakTracks2;
        
    end
    
    % track length filter
    peakTracks = peakTracks(cellfun(@length,peakTracks)>minTrackLength);
    % track linearity filter
    for j = length(peakTracks):-1:1
        gOfF = regstats(peakTracks{j}(:,1),peakTracks{j}(:,2),'linear','rsquare');
        if gOfF.rsquare < minTrackGof
            peakTracks(j) = [];
        end
    end
    
    % store results
    allTracks(end+1).track = currTrack;
    allTracks(end).kymograph = kymograph;
    allTracks(end).peaks = peaks;
    allTracks(end).peakTracks = peakTracks;
    allTracks(end).cellImages = cellImages;
    allTracks(end).peakDistances = peakDistances;
end

% remove empty first element
allTracks = allTracks(2:end);
% Analyze the features of Z rings in epfluorescence time lapses
%
% Program will prompt for an image file and a corresponding pill mesh file 
%
% Results: 
% allTracks: data for Z rings, tracked from formation to division
% structured array with the following fields:
%    track: coordinates of each tracked cell from pill mesh data, in 
%      [time, object] format
%    kymograph: intensity along the midline of the corresponding cell at
%      each time point
%    peaks: Z rings identified in each cell, in [time, coordinate along
%      cell midline, width, height] format
%    peakTracks: tracks of Z rings in each cell, in same format as peaks
%    cellImages: images of cells at each time point
%
% Code to display results is also provided

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

%% Parameter selection

% range around each Z ring to align, in pixels
windowSize = 15;
% minimum and maximum Z ring track length to analyze, in frames
minLength = 20; maxLength = 40;
% number of time steps to divide tracks into 
timeStep = 20;

%% Initialization 

% Add bioformats to path
addpath([pwd,'/bfmatlab/'])

% Initialize bioformats
status = bfCheckJavaPath;
assert(status, ['Missing Bio-Formats library. Either add bioformats_package.jar '...
    'to the static Java path or add it to the Matlab path.']);

%% Load files

% open single molecule time lapse
[file, path] = uigetfile(bfGetFileExtensions,'Open image file',path);
if file == 0
    return
end
img = bfopen(fullfile(path,file));
img = cat(3,img{1}{:,1});

% open pill mesh file
[file, path] = uigetfile('*pill_MESH.mat','Open image file',path);
if file == 0
    return
end
pillMesh = open(fullfile(path,file));
frame = pillMesh.frame;
Ncell = pillMesh.Ncell;

%% Track Z rings
allTracks = getTracks(img,frame,Ncell);

%% Compute average Z ring projections

% holder for aligned Z ring peaks
alignedPeaks = cell(timeStep,1);
temp = zeros(windowSize*2+1,1);
[alignedPeaks{:}] = deal(temp);

% holder for aligned Z ring images
alignedImages = cell(timeStep,1);
temp = zeros(windowSize*2+1,10);
[alignedImages{:}] = deal(temp);
    
for i = 1:length(allTracks)
    peakTracks = allTracks(i).peakTracks;
    kymograph = allTracks(i).kymograph;
    cellImage = allTracks(i).cellImages;
    for j = 1:length(peakTracks)
        currTrackLength = peakTracks{j}(end,1)-peakTracks{j}(1,1);
        % filter by length, discard tracks that overlap with start and end
        % of time lapse
        if peakTracks{j}(1,1) ~= 1 && peakTracks{j}(end,1) ~= 121 && currTrackLength >= minLength && currTrackLength <= maxLength
            % align peaks
            for k = 1:size(peakTracks{j},1)
                try % only if there's a large enough window size
                    currTimeIndex = discretize(peakTracks{j}(k,1)-peakTracks{j}(1)+1,linspace(0,peakTracks{j}(end,1)-peakTracks{j}(1,1),timeStep));
                    peakRegion = kymograph{peakTracks{j}(k,1)}(peakTracks{j}(k,2)-windowSize:peakTracks{j}(k,2)+windowSize);
                    alignedPeaks{currTimeIndex} = alignedPeaks{currTimeIndex}+peakRegion;
                    peakRegion = cellImage{peakTracks{j}(k,1)}(peakTracks{j}(k,2)-windowSize:peakTracks{j}(k,2)+windowSize,:);
                    alignedImages{currTimeIndex} = alignedImages{currTimeIndex}+peakRegion;
                catch ME
                    if ~strcmp(ME.identifier,'MATLAB:badsubscript')
                        rethrow(ME)
                    end
                end
            end
        end
    end
end

%% Plot peak widths over the cell cycle

allWidths = [];
for i = 1:timeStep-1
    [~,~,w,~] = findpeaks(alignedPeaks{i},'WidthReference','halfprom');
    allWidths(i) = max(w);
end

figure; hold on;

plot(linspace(0,1,length(allWidths(1,:))),allWidths(1,:),'LineWidth',3);

% plot aesthetics
set(gca,'LineWidth',2);set(gca,'FontSize',18);
set(gca,'TickDir','out');set(gca,'Box','off');
temp = xlabel('Normalized time (frames)'); temp.FontSize=30; temp.FontWeight='bold';
temp = ylabel('Z ring FWHM (pixels)'); temp.FontSize=30; temp.FontWeight='bold';

%% Show Z ring projections over the cell cycle

figure;
for i = 1:timeStep-1
    imshow(alignedImages{i}',[],'InitialMagnification',2000);
    pause(0.05)
end
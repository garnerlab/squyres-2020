% Viewer for lifetime results generated in lifetimePipeline
%
% Inputs
%   img: xyt image stack
%   ltResults: lifetime results structure
% 
% All spatial and temporal coordinates are expected in pixels and frames
%
% Returns: none

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

function viewLifetimes(img,ltResults)

%% Prepare initial image figure

% Count number of frames
NumberImages = size(img,3);

% Set marker size to 19*spanSize
ms = 19*3;

% Parse a spot coordinate list from ltResults
spotList = cat(1,ltResults(:).coords);

currSpot = 1; % start by showing first spot in image

% initialize variables that need scope for child functions
markerLine = gobjects(0); 
imCrop = []; 
corrSpotList = []; 
coord = [];
stateList = [];
onState = [];
imScales = [];

% Create figure
f = figure('Position',[100 230 1270 530],'ToolBar','none','MenuBar','none',...
    'NumberTitle','off','WindowKeyReleaseFcn',@goNext);
axis1 = axes('Units','pixels','Position',[50 80 405 405]);
colormap gray
axis2 = axes('Units','pixels','Position',[550 80 405 405]);

% Create slider for frame control
sldFrame = uicontrol('Style', 'slider','Min',1,'Max',NumberImages,'Value',1,'SliderStep',[1/NumberImages,10/NumberImages], ...
    'Units','pixels','Position', [50 40 405 20], 'Callback',@sldFrame_Callback);
% Listener for dynamic update on click and drag
addlistener(sldFrame,'ContinuousValueChange',@sldFrame_Callback);

% Create text box for stats
statBox = uicontrol('Style','text','String','Stats:','Units','pixels',...
    'Position',[1005 100 215 385],'FontSize',14);

% Draw first frame
redraw;

%% GUI callbacks and subfunctions

% Advance to next spot on button press
function goNext(varargin)
    currSpot = currSpot+1;
    if currSpot <= length(ltResults)
        redraw
    else
        close(f)
    end
end

% Draw new figure when current spot is updated
function redraw
    
    % Trim img to 51x51 pixel area centered on spot coordinate
    
    coord = ltResults(currSpot).coords;
    coord = round(coord);
    XPos = coord(1);
    YPos = coord(2);

    pixWindow = 25;
    imWidth = size(img,1);
    imHeight = size(img,2);
    tmpLeft = XPos-pixWindow;
    tmpRight = XPos+pixWindow;
    tmpBottom = YPos-pixWindow;
    tmpTop =  YPos+pixWindow;

    % error handling for edges of image
    tmpLeft = max(1,tmpLeft);
    tmpRight = min(tmpRight,imWidth);
    tmpBottom = max(1, tmpBottom);
    tmpTop = min(tmpTop, imHeight);

    imCrop = img(tmpLeft:tmpRight,tmpBottom:tmpTop,:);
    
    % scaling factor for imadjust
    temp = double(imCrop(:,:,1))./2^16;
    imScales = [prctile(temp(:),0),prctile(temp(:),100)];

    % correct coordinates for current spot and spot list
    coord(1) = coord(1)-tmpLeft+1;
    coord(2) = coord(2)-tmpBottom+1;
    corrSpotList = spotList;
    corrSpotList(currSpot,:) = [];
    corrSpotList(:,1) = corrSpotList(:,1)-tmpLeft+1;
    corrSpotList(:,2) = corrSpotList(:,2)-tmpBottom+1;
    
    % plot new trace
    trace = ltResults(currSpot).trace;
    stateList = ltResults(currSpot).states;
    onState = ltResults(currSpot).onState;
    
    axes(axis2)
    hold off
    plot(axis2,1:length(trace),trace,'LineWidth',2);
    hold on
    plot(axis2,1:length(stateList),stateList,'LineWidth',2);
    delete(markerLine)
    markerLine = plot(axis2,[1 1],get(axis2,'YLim'),'k','LineWidth',2);
    set(axis2,'LineWidth',2);set(axis2,'FontSize',16);
    set(axis2,'TickDir','out');set(axis2,'Box','off');
    xlabel('Frame')
    ylabel('Intensity')
    
    % plot new image and points
    axes(axis1)
    f.Name = ['Lifetime trace viewer: spot ',num2str(currSpot),' of ',num2str(length(ltResults)),' (press any key to advance)'];
    % draw new image
	set(sldFrame,'Value',1);
    sldFrame_Callback
    
    % update statistics
    maxCounts = max(trace);
    numStates = ltResults(currSpot).numStates;
    stateDiff = onState-ltResults(currSpot).offState;
    numDwells = length(ltResults(currSpot).lifetimes);
    
    statString = {['Spot ',num2str(currSpot),' of ',num2str(length(ltResults))],...
        '',['Maximum counts: ',num2str(max(trace))],...
        '',['Number of states: ',num2str(numStates)],...
        '',['State difference: ',num2str(stateDiff)],...
        '',['Number of on states: ',num2str(numDwells)],...
        '',['Lifetimes: ',num2str(ltResults(currSpot).lifetimes)]};
    
    set(statBox,'String',statString);
    
end
    

% Slider callback: updates displayed image when sliding in time
function sldFrame_Callback(varargin)
    
    % show current image
    slice = round(get(sldFrame,'Value'));
    currSlice = uint16(imCrop(:,:,slice));
    axes(axis1)
    hold off;
    imshow(imadjust(currSlice,imScales),'InitialMagnification',20);
    hold on;
    
    % plot points
    plot(axis1,corrSpotList(:,2),corrSpotList(:,1),'sb','MarkerSize',ms); 
    hold on
    % plot current point depending on state
    if stateList(slice) == onState
        plot(axis1,coord(2),coord(1),'sg','MarkerSize',ms); 
    else
        plot(axis1,coord(2),coord(1),'sr','MarkerSize',ms); 
    end
    
    % update scanning line on plot
    markerLine.XData = [slice slice];
    %}
end

end
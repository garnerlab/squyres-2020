% Computes single molecule lifetime from single molecule microscopy data
%
% Required packages: bioformats, vbFRET, importTrackMateTracks
%
% Program will prompt for a particle tracking file, a single particle time
% lapse, and a segmented binary mask of cells in the image (optional)
%
% Results: ltResults structure, with fields:
%   coords: centroid coordinates of spot
%   trace: background subtracted intensity trace
%   states: time vector of HMM states
%   lifetimes: lifetimes for on state events in the trace, in seconds
%   onState: intensity value corresponding to on state
%   offState: intensity value corresponding to off state

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

%% Initialization 

% Add bioformats to path
addpath([pwd,'/bfmatlab/'])
% Add vbFRET to path
addpath([pwd,'/vbFRET/'])
addpath([pwd,'/vbFRET/src'])
addpath([pwd,'/vbFRET/ext_src'])

% Initialize bioformats
status = bfCheckJavaPath;
assert(status, ['Missing Bio-Formats library. Either add bioformats_package.jar '...
    'to the static Java path or add it to the Matlab path.']);

%% Compute single molecule lifetime 

% open track file
[file,path] = uigetfile({'*.xml;*.mat'},'Open track file');
if file == 0
    return
end

% save file name and path for outputs
outPath = path;
temp = strsplit(file,'.');
outFile = [temp{1},'_ltResults.mat'];

% if .xml file selected, convert track file to .mat format and save
if strcmp(temp{2},'xml')
    tracks = importTrackMateTracks(fullfile(path,file));
    save(fullfile(path,[temp{1},'.mat']),'tracks');
else
    tracks = open(fullfile(path,file));
    tracks = tracks.tracks;
end

% open single molecule time lapse
[file, path] = uigetfile(bfGetFileExtensions,'Open image file',path);
if file == 0
    return
end
img = bfopen(fullfile(path,file));
img = cat(3,img{1}{:,1});

% open binary mask
[file, path] = uigetfile(bfGetFileExtensions,'Open binary mask (optional)',path);
if file == 0 % set all to 1 if no binary mask is found
    mask = ones(size(img,1),size(img,2)); 
else
    mask = bfopen(fullfile(path,file));
    mask = cat(3,mask{1}{:,1});
    % dilate mask
    mask = imdilate(mask>.5,strel('square',3));
end

%% compute single molecule lifetimes and save results
ltResults = getLifetimes(img,mask,tracks);
save(fullfile(outPath,outFile),'ltResults');

%% visualize the results
lifetimeView(img,ltResults);

% Spot selection, filtering, and HMM-based lifetime calculation
% 
% Inputs: 
%   img: xyt image stack
%   mask: binary mask for segmentation
%   tracks: particle tracking results from TrackMate
% 
% Outut: ltResults structure, with fields:
%   coords: centroid coordinates of spot
%   trace: background subtracted intensity trace
%   states: time vector of HMM states
%   lifetimes: lifetimes for on state events in the trace, in seconds
%   onState: intensity value corresponding to on state
%   offState: intensity value corresponding to off state

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

function ltResults = getLifetimes(img,mask,tracks)

% =========================== 
%       Key Parameters
% ===========================

timeInterval = 0.5; % interval between frames, in seconds

% for generating and filtering the intensity trace
spanSize = 2; % 1: 3x3 binning for intensity trace, 2: 5x5
bgSpanSize = spanSize + 2; % Span for local background subtraction
bgsLocal = 1; % set to 1 for local background subtraction, 0 for global
mergeDist = spanSize+1; % number of pixels between two centroids for them to be merged
threshCounts = 500; % minimum threshold for maximum intensity of intensity trace

% for HMM fitting
minStates = 1; % minimum number of HMM states to fit
maxStates = 4; % maximum number of HMM states to fit

% for filtering the HMM results
minStateDiff = 60; % minimum counts between on and off state
maxDwells = 2; % maximum number of on state events in one trace
minDwell = 2; % minimum dwell length in seconds 

% ===========================

%% Select spots and get initial intensity traces

% Create empty structure
ltResults = struct('coords',{},'trace',{},'states',{},'lifetimes',{},'onState',{},'offState',{});

% Create initial spot list by averaging positions along the track
spotList = zeros(length(tracks),2);
for i = 1:length(tracks)
    spotList(i,:) = mean(tracks{i}(:,3:-1:2),1)+1;
end

% Remove spots not inside cells
for i = size(spotList,1):-1:1
    if ~mask(round(spotList(i,1)),round(spotList(i,2)))
        spotList(i,:) = [];
    end
end

if isempty(spotList)
    return
end

% Get preliminary intensity traces
[spotList,~,maxList] = getTraces(img,spotList,spanSize,bgSpanSize,bgsLocal);

%% Filter and merge spots, get final intensity traces

fSpotList = spotList;

% remove intensity traces below intensity theshold
for i = length(fSpotList):-1:1
    if maxList(i) < threshCounts
        fSpotList(i,:) = [];
    end
end

if isempty(fSpotList)
    return
end

% merge spots: if two spots are within the thresold distance, average their
% positions, weighted by the length of each track
doneMerging = false;
weightList = ones(length(fSpotList),1); % weights for weighted averaging
while ~doneMerging
    % compute all euclidian distances
    % currDistances(i,j) = euclidian distance between i and j
    currDistances = squareform(pdist(fSpotList));
	% end if minimum distance is greater than merge cutoff
    minDistance = min(currDistances(currDistances>0));
    if size(fSpotList,1) == 1 || minDistance > mergeDist
        doneMerging = true;
    else
        % find indices of pair to be merged
        [i,j] = find(currDistances==minDistance);
        i = i(1); j = j(1); % fix when multiple pairs have the same distance
        % get new set of average coordinates 
        newWeight = (weightList(i)+weightList(j));
        newCoords = (fSpotList(i,:)*weightList(i)+fSpotList(j,:)*weightList(j))./newWeight;
        % remove old spots from list
        fSpotList([i,j],:) = [];
        weightList([i,j],:) = [];
        % add new merged spot to list
        fSpotList(end+1,:) = newCoords;
        weightList(end+1,:) = newWeight;
    end
end

spotList = fSpotList; clear fSpotList

% generate final intensity traces
[spotList,traces] = getTraces(img,spotList,spanSize,bgSpanSize,bgsLocal);
if isempty(spotList)
    return
end

%% Compute lifetimes 

for i=1:size(spotList,1)
    
    currTrace = traces(i,:);

    % fit HMMs and compute lifetimes
	[stateList,numStates,dwells,onState,offState] = getDwells(currTrace,minStates,maxStates);
    
    % discard if the best fit model does not have 2 states
    if numStates ~= 2 
        continue
    end
    
    % discard if the difference between on and off states is below
    % threshold
    if (onState-offState)<minStateDiff
        continue
    end
    
	% discard if there are too many on state events in the trace
    if length(dwells)>maxDwells
        continue
    end
    
    % discard dwells less than 4 frames (2 seconds) 
    dwells = dwells.*timeInterval;
    dwells = dwells(dwells>=minDwell);
    
    % store all data in hmmResults structure
    ltResults(end+1).numStates = numStates;
    ltResults(end).coords = spotList(i,:);
    ltResults(end).trace = currTrace;
    ltResults(end).states = stateList;
    ltResults(end).lifetimes = dwells;
    ltResults(end).onState = onState;
    ltResults(end).offState = offState;

end
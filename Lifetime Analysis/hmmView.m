% HMM result viewer for intensity traces and resulting HMM fits
% Called from interactive lifetime pipeline
%
% Inputs
%   img: Image stack
%   currSpot: index of current spot in spot list
%   spotList: list of spots found in image
%   trace: intensity trajectory
%   stateList: list of states for HMM
%   onState: value of on state from HMM
%   spanSize: Span size for spot
% 
% All spatial and temporal coordinates are expected in pixels and frames
%
% Returns
%   f: handle to figure window
%   statBox: handle to text box in figure

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

function [f,statBox] = hmmView(Img,currSpot,spotList,trace,stateList,onState,spanSize)

%% Initialization

% Set marker size
ms = 19*(spanSize+1);

% Count number of frames
NumberImages = size(Img,3);

% scaling factor for imadjust
temp = double(Img(:,:,1))./2^16;
imScales = [prctile(temp(:),0),prctile(temp(:),100)];

global hmmViewInteractiveQuit
hmmViewInteractiveQuit = false;

%% Trim img to 50x50 pixel area centered on spot coordinate

coord = spotList(currSpot,:);
spotList(currSpot,:) = [];
coord = round(coord);
XPos = coord(1);
YPos = coord(2);

pixWindow = 25;
imWidth = size(Img,1);
imHeight = size(Img,2);
tmpLeft = XPos-pixWindow;
tmpRight = XPos+pixWindow;
tmpBottom = YPos-pixWindow;
tmpTop =  YPos+pixWindow;

% error handling for edges of image
tmpLeft = max(1,tmpLeft);
tmpRight = min(tmpRight,imWidth);
tmpBottom = max(1, tmpBottom);
tmpTop = min(tmpTop, imHeight);

Img = Img(tmpLeft:tmpRight,tmpBottom:tmpTop,:);

% correct coordinates
coord(1) = coord(1)-tmpLeft+1;
coord(2) = coord(2)-tmpBottom+1;
spotList(:,1) = spotList(:,1)-tmpLeft+1;
spotList(:,2) = spotList(:,2)-tmpBottom+1;

%% Create figures

% Create axes
f = figure('Position',[100 230 1270 530],'ToolBar','none','MenuBar','none',...
    'NumberTitle','off','Name','Trace Viewer (press any key to advance)','WindowKeyReleaseFcn','close');
axis1 = axes('Units','pixels','Position',[50 80 405 405]);
colormap gray
axis2 = axes('Units','pixels','Position',[550 80 405 405]);

% Plot intensity trace and HMM states
plot(axis2,1:length(trace),trace,'LineWidth',2);
hold on
plot(axis2,1:length(stateList),stateList,'LineWidth',2);
markerLine = plot(axis2,[1 1],get(axis2,'YLim'),'k','LineWidth',2);

set(axis2,'LineWidth',2);set(axis2,'FontSize',16);
set(axis2,'TickDir','out');set(axis2,'Box','off');
xlabel('Frame')
ylabel('Intensity')

% Create slider for frame control
sldFrame = uicontrol('Style', 'slider','Min',1,'Max',NumberImages,'Value',1,'SliderStep',[1/NumberImages,10/NumberImages], ...
    'Units','pixels','Position', [50 40 405 20], 'Callback',@sldFrame_Callback);
% Listener for dynamic update on click and drag
addlistener(sldFrame,'ContinuousValueChange',@sldFrame_Callback);

% Create quit button
uicontrol('Style','pushbutton','String','Quit','FontSize',18,'Units','pixels',...
    'Position',[1072 40 81 40],'Callback',@quitBtn_Callback)

% Create text box for stats
statBox = uicontrol('Style','text','String','Stats:','Units','pixels',...
    'Position',[1005 100 215 385],'FontSize',18);

sldFrame_Callback;

%% Slider callback

function sldFrame_Callback(varargin)
    % show current image
	slice = round(get(sldFrame,'Value'));
    currSlice = uint16(Img(:,:,slice));
    axes(axis1)
    hold off;
    imshow(imadjust(currSlice,imScales),'InitialMagnification',20);
    hold on;
    plot(axis1,spotList(:,2),spotList(:,1),'sb','MarkerSize',ms);
    if stateList(slice) == onState
        plot(axis1,coord(2),coord(1),'sg','MarkerSize',ms); 
    else
        plot(axis1,coord(2),coord(1),'sr','MarkerSize',ms); 
    end
    % update scanning line on plot
    markerLine.XData = [slice slice];
end

function quitBtn_Callback(varargin)
    hmmViewInteractiveQuit = true;
    close(f)
end

end
% Command line calling of vbFRET for lifetime fitting and dwell time analysis
% This is a modified version of the vbFRET command line code
%
% Input: 
%   trace: intensity trace as a row vector
%   kmin: minimum number of HMM states to fit
%   K: maximum number of HMM states to fit
% 
% Returns:
%   stateValueList: HMM state value at each time point
%   numStates: number of HMM states for best fit
%
%   if numStates == 2, also returns: 
%   dwells: list of dwell times for the on state
%   onState: state value corresponding to the on (high intensity) state
%   offState: state value corresponding to the off (low intensity) state
%
%   if numStates ~= 2 or if errors are thrown, default values are 
%   onState = -1, offState = -1, dwells = []
% 
% Note: dwell times that overlap with the start or end of the trace are
% discarded 

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

function [stateValueList,numStates,dwells,onState,offState] = getDwells(trace,kmin,K)

%% parameter settings

% analyze data in 1D
D = 1;
% set maximum number of restarts
I = 10;

% analyzeFRET program settings
PriorPar.upi = 1;
PriorPar.mu = .5*ones(D,1);
PriorPar.beta = 0.25;
PriorPar.W = 50*eye(D);
PriorPar.v = 5.0;
PriorPar.ua = 1.0;
PriorPar.uad = 0;
%PriorPar.Wa_init = true;

% set the vb_opts for VBEM
% stop after vb_opts iterations if program has not yet converged
vb_opts.maxIter = 100;
% question: should this be a function of the size of the data set??
vb_opts.threshold = 1e-5;
% display graphical analysis
vb_opts.displayFig = 0;
% display nrg after each iteration of forward-back
vb_opts.displayNrg = 0;
% display iteration number after each round of forward-back
vb_opts.displayIter = 0;
% display number of steped needed for convergance
vb_opts.DisplayItersToConverge = 0;

% bestMix = cell(1,K);
bestOut=cell(1,K);
outF=-inf*ones(1,K);
best_idx=zeros(1,K);

%% Run the VBEM algorithm

trace = trace(:)'; %make sure the data is a row vector
for k=kmin:K
    ncentres = k;
    init_mu = (1:ncentres)'/(ncentres+1);
    i=1;
    maxLP = -Inf;

    while i<I+1
        if k==1 && i > 3
            break
        end
        if i > 1
            init_mu = rand(ncentres,1);
        end
        clear mix out;
        % Initialize gaussians
        % Note: x and mix can be saved at this point andused for future
        % experiments or for troubleshooting. try-catch needed because
        % sometimes the K-means algorithm doesn't initialze and the program
        % crashes otherwise when this happens.
        try
            [mix] = get_gmm_mix(trace',init_mu);
            [out] = vbFRET_VBEM(trace, mix, PriorPar, vb_opts);
        catch ME 
            disp('There was an error, repeating restart.');
            disp(ME.message)
            continue
        end

        % Only save the iterations with the best out.F
        if out.F(end) > maxLP
            maxLP = out.F(end);
            bestOut{k} = out;
            outF(k)=out.F(end);
            best_idx(k) = i;
        end
        i=i+1;
    end
end

%% VBHMM postprocessing and dwell time fitting

% Computes all dwell times for all transitions except for those at the 
% start or the end of the trace

if ~isempty(bestOut)
    
    % Choose overall best fit to assign state number
    [~,numStates] = max(cellfun(@(x) x.F(end),bestOut));

    % Get idealized data fits
    [stateList,stateValueList] = chmmViterbi(bestOut{numStates},trace);
    
    % check number of states
    if numStates ~= 2
        onState = -1;
        offState = -1;
        dwells = [];
        return
    end
    
    % Compute dwell times for state 1
    state1MatchIndex = find(stateList == 1); % find where trace is in state 1
    if isempty(state1MatchIndex)
        onState = -1;
        offState = -1;
        stateMeans = [];
        dwells = [];
        return
    end
    state1Value = stateValueList(state1MatchIndex(1)); % get state value
    % Deal with start and end
    if state1MatchIndex(1) ~= 1
        state1MatchIndex = [1,state1MatchIndex];
    end
    if state1MatchIndex(end) ~= length(trace)
        state1MatchIndex(end+1) = length(trace);
    end
    % Find gaps in match index
    state1Index = find(diff(state1MatchIndex)~=1); 
    % Compute difference for dwell time
    state1Dwells = diff(state1Index);

    % Repeat for state 2
    state2MatchIndex = find(stateList == 2); % find where trace is in state 2
    if isempty(state2MatchIndex)
        onState = -1;
        offState = -1;
        dwells = [];
        return
    end
    state2Value = stateValueList(state2MatchIndex(1)); % get state value
	% Deal with start and end
    if state2MatchIndex(1) ~= 1
        state2MatchIndex = [1,state2MatchIndex];
    end
    if state2MatchIndex(end) ~= length(trace)
        state2MatchIndex(end+1) = length(trace);
    end
    % Find gaps in match index
    state2Index = find(diff(state2MatchIndex)~=1); 
    % Compute difference for dwell time
    state2Dwells = diff(state2Index);
    
    % Identify which is the on state and assign outputs
    if state1Value > state2Value
        onState = state1Value;
        offState = state2Value;
        dwells = state1Dwells;
    else
        onState = state2Value;
        offState = state1Value;
        dwells = state2Dwells;
    end
else 
    onState = -1;
    offState = -1;
    dwells = [];
end

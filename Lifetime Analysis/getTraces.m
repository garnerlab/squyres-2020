% Get intensity traces from spots
% Intensity is binned around the spot, and then corrected for background in
% either local or global mode
% Discards spots that are too close to the edge of the image to allow for a
% full-size bin
%
% Inputs: 
%   img: xyt image stack
%   spotList: list of coordinates for spots in image
%   spanSize: pixel span size for intensity bin
%   bgSpanSize: pixel span size for background bin (for local subtraction)
%   bgsLocal: set to 1 for local background subtraction, 2 for global
% 
% Span size is the number of pixels away from the rounded centroid position
% that will be included. The bin width and height are spanSize*2+1
% 
% Outputs: 
%   spotList: list of coordinates for spots in image, edge spots removed
%   bgsTraces: background subtracted intensity traces
%   maxList: list of maximum intensities of each spot (normalized to bg)
%   rawTraces: intensity traces, no bgs (for troubleshooting)
%   bgTraces: background traces (for troubleshooting)

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

function [spotList,bgsTraces,maxList,rawTraces,bgTraces] = getTraces(img,spotList,spanSize,bgSpanSize,bgsLocal)

imWidth = size(img,1);
imHeight = size(img,2);
nFrames = size(img,3);
nSpots = size(spotList,1);

%% Discard spots that are too close to the edge of the image
if bgsLocal
    minEdgeDistance = bgSpanSize;
else
    minEdgeDistance = spanSize;
end

for i = nSpots:-1:1
    
	XPos=round(spotList(i,1));
	YPos=round(spotList(i,2));
    
    tmpLeft = XPos-minEdgeDistance;
    tmpRight = XPos+minEdgeDistance;
    tmpBottom = YPos-minEdgeDistance;
    tmpTop =  YPos+minEdgeDistance;
    
    % Remove from spot list if too close to the edge
    if tmpLeft < 1 || tmpRight > imWidth || tmpBottom < 1 || tmpTop > imHeight
        spotList(i,:) = [];
    end
end

nSpots = size(spotList,1);
frameBkgd = zeros(nFrames,1); % global background

bgsTraces = zeros(nSpots,nFrames); % final traces
rawTraces = zeros(nSpots,nFrames); % raw traces
bgTraces = zeros(nSpots,nFrames); % raw background traces
maxList = zeros(nSpots,1); % max intensity in each trace

for i = 1:nFrames

    currFrame = img(:,:,i);
    frameBkgd(i) = mean(currFrame(:));

    for j=1:nSpots

        % assign span coordinates
        XPos=round(spotList(j,1));
        YPos=round(spotList(j,2));
        tmpLeft = XPos-spanSize;
        tmpRight = XPos+spanSize;
        tmpBottom = YPos-spanSize;
        tmpTop =  YPos+spanSize;

        % calculate average intensity for this frame
        spotPixels = currFrame(tmpLeft:tmpRight,tmpBottom:tmpTop);
        spotSum = sum(spotPixels(:));
        spotPixelCount = numel(spotPixels);
        spotNorm = spotSum/spotPixelCount;

        rawTraces(j,i) = spotNorm;
        
        if bgsLocal % local background subtraction mode
            
            % repeat process with background span
            tmpLeft = XPos-bgSpanSize;
            tmpRight = XPos+bgSpanSize;
            tmpBottom = YPos-bgSpanSize;
            tmpTop =  YPos+bgSpanSize;
            
            % take a pixel border around the spot as background
            bgPixels = currFrame(tmpLeft:tmpRight,tmpBottom:tmpTop);
            bgSum = sum(bgPixels(:))-spotSum;
            % normalize for difference in pixel number between spot and bg
            bgPixelCount = numel(bgPixels)-spotPixelCount;
            bgNorm = bgSum/(bgPixelCount);
            
            bgsTraces(j,i) = spotNorm - bgNorm;
            bgTraces(j,i) = bgNorm;
            
            % update max intensity
            currentMax = max(spotPixels(:))-bgNorm;
            if currentMax > maxList(j)
                maxList(j) = currentMax;
            end

        else % global background subtraction mode
            
            bgsTraces(j,i) = spotSum-frameBkgd(i);
            bgTraces(j,i) = frameBkgd(i);
            
            % update max intensity
            currentMax = max(spotPixels(:))-frameBkgd(i);
            if currentMax > maxList(j)
                maxList(j) = currentMax;
            end
            
        end

    end
end
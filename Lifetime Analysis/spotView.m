% Spot viewer to check background subtraction process
% Called from interactive lifetime pipeline
%
% Inputs
%   img: Image stack
%   coord: Coordinates of current spot from spot list
%   spotList: List of spots detected in the image
%   rawTrace: Unprocessed intensity trace from getTraces
%   bgTrace: Background trace from getTraces
%   bgsTrace: Background subtracted trace from getTraces
%   spanSize: Span size for spot
%   bgSpanSize: Span size for background
% 
% All spatial and temporal coordinates are expected in pixels and frames
%
% Returns
%   f: handle to figure window
%   statBox: handle to text box in figure

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

function [f,statBox] = spotView(Img,currSpot,spotList,rawTrace,bgTrace,bgsTrace,spanSize,bgSpanSize)

%% Initialization

% Set marker sizes
ms = 19*(spanSize+1);
bgms = 19*(bgSpanSize+1);

% Count number of frames
NumberImages = size(Img,3);

% scaling factor for imadjust
temp = double(Img(:,:,1))./2^16;
imScales = [prctile(temp(:),0),prctile(temp(:),100)];

global hmmViewInteractiveQuit
hmmViewInteractiveQuit = false;

%% Trim img to 50x50 pixel area centered on spot coordinate

coord = spotList(currSpot,:);
spotList(currSpot,:) = [];
coord = round(coord);
XPos = coord(1);
YPos = coord(2);

pixWindow = 25;
imWidth = size(Img,1);
imHeight = size(Img,2);
tmpLeft = XPos-pixWindow;
tmpRight = XPos+pixWindow;
tmpBottom = YPos-pixWindow;
tmpTop =  YPos+pixWindow;

% error handling for edges of image
tmpLeft = max(1,tmpLeft);
tmpRight = min(tmpRight,imWidth);
tmpBottom = max(1, tmpBottom);
tmpTop = min(tmpTop, imHeight);

Img = Img(tmpLeft:tmpRight,tmpBottom:tmpTop,:);

% correct coordinates
coord(1) = round(coord(1))-tmpLeft+1;
coord(2) = round(coord(2))-tmpBottom+1;
spotList(:,1) = spotList(:,1)-tmpLeft+1;
spotList(:,2) = spotList(:,2)-tmpBottom+1;

%% Create figure

% Create axes
f = figure('Position',[100 230 1270 530],'ToolBar','none','MenuBar','none',...
    'NumberTitle','off','Name','Spot Viewer (press any key to advance)','WindowKeyReleaseFcn','close');
axis1 = axes('Units','pixels','Position',[50 80 405 405]);
colormap gray

% Plot intensity traces
axisRaw = axes('Units','pixels','Position',[550 384 405 122]);
plot(axisRaw,1:length(rawTrace),rawTrace,'LineWidth',2);
hold on; title('Raw Trace');
markerLine(1) = plot(axisRaw,[1 1],get(axisRaw,'YLim'),'k','LineWidth',1);

axisBG = axes('Units','pixels','Position',[550 202 405 122]);
plot(axisBG,1:length(bgTrace),bgTrace,'LineWidth',2);
hold on; title('Background Trace')
markerLine(2) = plot(axisBG,[1 1],get(axisBG,'YLim'),'k','LineWidth',1);

axisBGS = axes('Units','pixels','Position',[550 40 405 122]);
plot(axisBGS,1:length(bgsTrace),bgsTrace,'LineWidth',2);
hold on; title('Background Subtracted Trace')
markerLine(3) = plot(axisBGS,[1 1],get(axisBGS,'YLim'),'k','LineWidth',1);

% Create slider for frame control
sldFrame = uicontrol('Style', 'slider','Min',1,'Max',NumberImages,'Value',1,'SliderStep',[1/NumberImages,10/NumberImages], ...
    'Units','pixels','Position', [50 40 405 20], 'Callback',@sldFrame_Callback);
% Listener for dynamic update on click and drag
addlistener(sldFrame,'ContinuousValueChange',@sldFrame_Callback);

% Create text box for stats
statBox = uicontrol('Style','text','String','Stats:','Units','pixels',...
    'Position',[1005 100 215 385],'FontSize',18);

% Create quit button
uicontrol('Style','pushbutton','String','Quit','FontSize',18,'Units','pixels',...
    'Position',[1072 40 81 40],'Callback',@quitBtn_Callback)

sldFrame_Callback;

%% Slider callback

function sldFrame_Callback(varargin)
    % show current image
	slice = round(get(sldFrame,'Value'));
    currSlice = uint16(Img(:,:,slice));
    axes(axis1)
    hold off;
    imshow(imadjust(currSlice,imScales),'InitialMagnification',20);
    hold on;
    % reverse XY coords when plotting
    plot(axis1,spotList(:,2),spotList(:,1),'sb','MarkerSize',ms);
    plot(axis1,coord(2),coord(1),'sg','MarkerSize',ms); 
	plot(axis1,coord(2),coord(1),'sy','MarkerSize',bgms); 
    % update scanning line on plot
    markerLine(1).XData = [slice slice];
    markerLine(2).XData = [slice slice];
    markerLine(3).XData = [slice slice];
end

function quitBtn_Callback(varargin)
    hmmViewInteractiveQuit = true;
    close(f)
end

end
README: Single molecule lifetime analysis

WRITTEN BY: Georgia Squyres, Garner Lab, Harvard University
In MATLAB R2018b
Contact: squyres@g.harvard.edu 

ADDITIONAL DOWNLOADS: 

In order to run this code, you must additionally download the following and add them to your MATLAB path:

importTrackMateTracks: https://github.com/fiji/TrackMate/blob/master/scripts/importTrackMateTracks.m
bfmatlab: https://downloads.openmicroscopy.org/bio-formats/
vbfret: http://vbfret.sourceforge.net/


INSTRUCTIONS FOR USE:

First, generate particle tracks in TrackMate, and use the “Export tracks to XML file” option after particle tracking. Optionally, you can also segment cells to generate cell masks using DeepCell or the cell segmentation program of your choice. 

To run the single molecule lifetime pipeline on an image, run lifetimePipeline.m. You will first be prompted to load a tracks file. The first time you run the pipeline on an image, select the .xml file from TrackMate and it will automatically be read and saved as a .mat file. In the future, you can select the corresponding .mat file, which will load more quickly. Then, you will be prompted to open the image file, and, if desired, the mask file from cell segmentation (if you do not have a mask file just press cancel at this prompt). The program will automatically compute single molecule lifetimes for your image and save the results structure as [track file name]_ltResults.mat. There are several important parameters used throughout the process that you may want to specify for your data set; they are listed at the top of getLifetimes.m in the key parameters section. Once you are done, you can visualize the results using lifetimeView (simply uncomment the last line).

The ltResults structure contains the following fields:
coords: centroid coordinates of each spot
trace: background subtracted intensity trace
states: time vector of HMM states
lifetimes: lifetimes for on state events in the trace, in seconds
onState: intensity value corresponding to on state
offState: intensity value corresponding to off state

It is also possible to run the analysis in an interactive mode in order to visualize the results of each step. This can be helpful to select the threshold parameter values you want to use for your data set. To use this mode, run each section of interactivePipeline.m in order. 
% Interactive pipeline for lifetime measurements with spot detection
% Use this pipeline to check the performance of spot detection, background
% subtraction, spot filtering, and HMM fitting
%
% Calls viewers to visualize the results dynamically: 
%    spotView for spot selection and background subtraction
%    hmmView for HMM fitting

% Written in MATLAB R2018b
% Georgia Squyres
% squyres@g.harvard.edu

%% Parameter selection

% =========================== 
%       Key Parameters
% ===========================

% Trace generation parameters:
spanSize = 2; % 1: 3x3 binning for trace, 2: 5x5
bgSpanSize = spanSize + 2; % Span for local BG subtraction
bgsLocal = 1; % set to 1 for local background subtraction, 0 for global
mergeDist = spanSize + 1; % number of pixels between two centroids for them to be merged
smoothSize = 0; 

%% Initialization 

% Add bioformats to path
addpath([pwd,'/bfmatlab/'])
% Add vbFRET to path
addpath([pwd,'/vbFRET/'])
addpath([pwd,'/vbFRET/src'])
addpath([pwd,'/vbFRET/ext_src'])

% Initialize bioformats
status = bfCheckJavaPath;
assert(status, ['Missing Bio-Formats library. Either add bioformats_package.jar '...
    'to the static Java path or add it to the Matlab path.']);

%% File loading

% open track file
[file,path] = uigetfile({'*.xml;*.mat'},'Open track file');
if file == 0
    return
end

% if .xml file selected, convert track file to .mat format and save
if strcmp(temp{2},'xml')
    tracks = importTrackMateTracks(fullfile(path,file));
    save(fullfile(path,[temp{1},'.mat']),'tracks');
else
    tracks = open(fullfile(path,file));
    tracks = tracks.tracks;
end

% open single molecule time lapse
[file, path] = uigetfile(bfGetFileExtensions,'Open image file',path);
if file == 0
    return
end
img = bfopen(fullfile(path,file));
img = cat(3,img{1}{:,1});

% open binary mask
[file, path] = uigetfile(bfGetFileExtensions,'Open binary mask (optional)',path);
if file == 0 % set all to 1 if no binary mask is found
    mask = ones(size(img,1),size(img,2)); 
else
    mask = bfopen(fullfile(path,file));
    mask = cat(3,mask{1}{:,1});
    % dilate mask
    mask = imdilate(mask>.5,strel('square',3));
end
%%
imWidth = size(img,1);
imHeight = size(img,2);
nFrames = size(img,3);

% Initialization


% ===========================

% Declare global variable to quit the GUI
global hmmViewInteractiveQuit

% Create initial spot list 
spotList = zeros(length(tracks),2);
for i = 1:length(tracks)
    spotList(i,:) = mean(tracks{i}(:,3:-1:2),1)+1; 
end

% Remove spots not inside cells
for i = size(spotList,1):-1:1
    if ~imgMask(round(spotList(i,1)),round(spotList(i,2)))
        spotList(i,:) = [];
    end
end

% Get traces
[spotList,bgsTraces,maxList,rawTraces,bgTraces] = getTraces(img,spotList,spanSize,bgSpanSize,bgsLocal);
%
% View initial spots

figure
mip = max(img,[],3);
temp = double(mip)./2^16;
imshow(imadjust(mip,[prctile(temp(:),0),prctile(temp(:),99.99)]),'InitialMagnification',300); 
colormap gray
hold on
plot(spotList(:,2),spotList(:,1),'or') % reverse XY coords when plotting
%}

%% View spots and spot features

hmmViewInteractiveQuit = false;

for i=1:size(spotList,1)
    
    currTrace = bgsTraces(i,:);
	%currTrace = smooth(currTrace,smoothSize); % smooth to denoise
    %currTrace = max(currTrace,0); % remove negatives
    
    [f,statBox] = spotView(img,i,spotList,rawTraces(i,:),...
        bgTraces(i,:),bgsTraces(i,:),spanSize,bgSpanSize);
    
    % Compute statistics to show in stats box
    peakHeight = maxList(i);
    maxCounts = max(currTrace);
    contrastRatio = (max(currTrace)-mean(currTrace))/std(currTrace);
    
    statString = {['Spot ',num2str(i),' of ',num2str(size(spotList,1))],...
        '',['Peak height: ',num2str(peakHeight)],...
        '',['Maximum counts: ',num2str(maxCounts)],...
        '',['Contrast ratio: ',num2str(contrastRatio)]};
    
    set(statBox,'String',statString);
    
    uiwait(f)

    if hmmViewInteractiveQuit
        break
    end
    
end

%% Test filters on image overlay

% show image
figure
mip = max(img,[],3);
temp = double(mip)./2^16;
imshow(imadjust(mip,[prctile(temp(:),0),prctile(temp(:),99.99)]),'InitialMagnification',300); 
colormap gray
hold on

passSpots = []; % spots that pass filtering
failSpots = []; % spots that do not pass filtering

threshCounts = 500;
threshContrastRatio = 0;

% filter spots
for i = 1:size(spotList,1)
    
	currTrace = bgsTraces(i,:);
    %currTrace = smooth(currTrace,smoothSize);
    %currTrace = max(currTrace,0); % remove negatives
    
	peakHeight = maxList(i);
    countsAbove = max(currTrace);
    contrastRatio = (max(currTrace)-mean(currTrace))/std(currTrace);
    
    if peakHeight >= threshCounts && contrastRatio >= threshContrastRatio
        passSpots = [passSpots;spotList(i,:)];
    else
        failSpots = [failSpots;spotList(i,:)];
    end
end

% plot
if ~isempty(failSpots)
failPlot = scatter(failSpots(:,2),failSpots(:,1),80,'MarkerEdgeColor','none',...
	'MarkerFaceColor','r'); failPlot.MarkerFaceAlpha = 0.2;
end
if ~isempty(passSpots)
passPlot = scatter(passSpots(:,2),passSpots(:,1),80,'MarkerEdgeColor','none',...
	'MarkerFaceColor','g'); passPlot.MarkerFaceAlpha = 0.2;
end
    
%% Filter, merge spots, and compute HMMs

% Filtering

threshCounts = 500;
threshContrastRatio = 0;

fSpotList = spotList;

for i = length(fSpotList):-1:1
    
	currTrace = bgsTraces(i,:);
    %currTrace = smooth(currTrace,smoothSize);
    %currTrace = max(currTrace,0); % remove negatives
    
	peakHeight = maxList(i);
    contrastRatio = (max(currTrace)-mean(currTrace))/std(currTrace);
    
    if peakHeight < threshCounts || contrastRatio < threshContrastRatio
        fSpotList(i,:) = [];
    end
end

% Merge spots

doneMerging = false;
weightList = ones(length(fSpotList),1); % weights for weighted averaging
while ~doneMerging
    % compute all euclidian distances
    % currDistances(i,j) = euclidian distance between i and j
    currDistances = squareform(pdist(fSpotList));
	% end if minimum distance is greater than merge cutoff
    minDistance = min(currDistances(currDistances>0));
    if minDistance > mergeDist
        doneMerging = true;
    else
        % find indices of pair to be merged
        [i,j] = find(currDistances==minDistance);
        i = i(1); j = j(1); % fix when multiple pairs have the same distance
        % get new set of average coordinates 
        newWeight = (weightList(i)+weightList(j));
        newCoords = (fSpotList(i,:)*weightList(i)+fSpotList(j,:)*weightList(j))./newWeight;
        % add new merged spot to list
        fSpotList(i,:) = newCoords;
        weightList(i,:) = newWeight;
        % remove old spot from list
        fSpotList(j,:) = [];
        weightList(j,:) = [];
    end
end

% Show final spot image
imFig = figure;
mip = max(img,[],3);
temp = double(mip)./2^16;
imshow(imadjust(mip,[prctile(temp(:),0),prctile(temp(:),99.99)]),'InitialMagnification',300); 
colormap gray
hold on
plot(fSpotList(:,2),fSpotList(:,1),'or')
currSpotHandle = gobjects(0);
drawnow

% Generate final intensity traces

[fSpotList,fBGSTraces,fMaxList] = getTraces(img,fSpotList,spanSize,bgSpanSize,bgsLocal);

% Compute HMMs

hmmViewInteractiveQuit = false;

for i=1:size(fSpotList,1)
    
    currTrace = fBGSTraces(i,:);
    %currTrace = smooth(fBGSTraces(i,:),smoothSize); % smooth out noise
    %currTrace = max(currTrace,0); % remove negatives

    % normalization
    %traceAmplitude = max(10,median(currTrace));
    %currBkgd = ones(nFrames,1).*traceAmplitude;
    %currTrace = currTrace./(currBkgd+currTrace);
    
	if sum(currTrace) == 0
        continue
    end
    
    % Compute HMM
	[stateList,numStates,dwells,onState,offState] = hmmDwells(currTrace,1,4);

    
    % Plot current point on image figure in green
    delete(currSpotHandle);
    currSpotHandle=plot(fSpotList(i,2),fSpotList(i,1),'og');
    
    % Show results
    if ~isempty(dwells) && numStates == 2
        [f,statBox] = hmmView(img,i,fSpotList,currTrace,stateList,onState,spanSize);
    else
        [f,statBox] = hmmView(img,i,fSpotList,currTrace,stateList,-1,spanSize);
    end
    
    % Compute statistics to show in stats box
    peakHeight = fMaxList(i);
    maxCounts = max(currTrace);
    contrastRatio = (max(currTrace)-mean(currTrace))/std(currTrace);
    if ~isempty(dwells)
        stateRatio = onState-offState;
        numDwells = length(dwells);
    else
        stateRatio = -1;
        numDwells = -1;
    end
    
    statString = {['Spot ',num2str(i),' of ',num2str(size(fSpotList,1))],...
        '',['Peak height: ',num2str(peakHeight)],...
        '',['Maximum intensity: ',num2str(maxCounts)],...
        '',['Contrast ratio: ',num2str(contrastRatio)],...
        '',['Number of states: ',num2str(numStates)],...
        '',['State difference: ',num2str(stateRatio)],...
        '',['Number of dwells: ',num2str(numDwells)]};
    
    set(statBox,'String',statString);
    
    uiwait(f)

    if hmmViewInteractiveQuit
        break
    end
    
end